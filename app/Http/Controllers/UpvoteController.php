<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UpvoteRepository;

class UpvoteController extends Controller
{
    protected $upvoteRepository;

    public function __construct(UpvoteRepository $upvoteRepository)
    {
    $this->upvoteRepository = $upvoteRepository;

    $this->middleware("auth")->except("index");
    }

    public function store(Request $request)
    {

    $requestData = $request->all();
    $requestData ['user_id'] = auth()->user()->id;
    $this->upvoteRepository->create($requestData);

    return back();
    }
}
