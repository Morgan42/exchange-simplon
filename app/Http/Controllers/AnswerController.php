<?php

namespace App\Http\Controllers;

use App\Repositories\AnswerRepository;
use App\Repositories\QuestionRepository;
use App\Http\Requests\StoreAnswer;
use App\Http\Requests\StoreQuestion;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    protected $answerRepository;

    public function __construct(AnswerRepository $answerRepository)
    {
        $this->answerRepository = $answerRepository;
    }

    public function store(Request $request)
    {
        $this->middleware('auth');

        $requestData = $request->all();
        $requestData ['user_id'] = auth()->user()->id;

        $this->answerRepository->create($requestData);
        
        return back();
    }
}
