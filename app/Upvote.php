<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upvote extends Model
{
    protected $table = "up_vote";
    protected $fillable = ["question_id", "user_id", "answer_id"];
    protected $dates = ["created_at", "updated_at"];

    public function user()
    {
    return $this->belongsTo("App\User");
    }
}
