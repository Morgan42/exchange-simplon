<?php namespace App\Repositories;

use App\Answer;
use Illuminate\Database\Eloquent\Model;

class AnswerRepository extends Repository
{
    public function __construct(Answer $model)
    {
        parent::__construct($model);
    }

    public function getOrderedAnswers($questionId)
    {        
        return $this->model->orderBy('updated_at', 'desc')->where('question_id', 'questionId')->get();
    }

    public function getAnswers()
    {
        return $this->model->all();
    }


}