@extends('layouts.fullwidth')

@section('title', 'Poser une question')

@section('content')

    <div class="page-content ask-question">
        <div class="boxedtitle page-title"><h2>Ask Question</h2></div>
        <p>Duis dapibus aliquam mi, eget euismod sem scelerisque ut. Vivamus at elit quis urna adipiscing iaculis. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque.</p>
        <div class="form-style form-style-3" id="question-submit">

            <!-- Ouverture du Formulaire-->
            {!! Form::open(array('action' => 'QuestionController@store', 'method' => 'post')) !!}
                @csrf
                {!!Form::token();!!}
                <div class="form-inputs clearfix">

            <!--Les titres -->
                <p>
                    {!!Form::label('title', 'Question Title');!!}
                    {!!Form::text('title', 'Titre', ['id'=>'question-title']);!!}
                    <span class="form-description">Please choose an appropriate title for the question to answer it even easier .</span>
                 </p>
            <!-- Les Tags -->
                <p>
                    {!!Form::label('text', 'Tags');!!}
                    {!!Form::text('text', null, ['id'=>'question_tags', 'class' => 'input', 'data-seperator' => ',']);!!}
                    <span class="form-description">Please choose suitable Keywords .</span>
                </p>
                
            <!-- Les catégories -->
                <p>
                    {!!Form::label('text', 'Category', ['id'=>'question_tags', 'class' => 'required']);!!}

                     <span class="styled-select" id="question-title"></span>

                    {!! Form::select('category',[
                                    'Back-end' => [
                                                    'php' => 'PHP',
                                                    'mysql' => 'MySQL',
                                                    'nodejs' => 'NodeJS',
                                                    'c#' => 'C#',
                                                    'python' => 'Python',
                                                    'ruby' => 'Ruby',
                                                    'other' => 'Other'
                                    ],
                                    'Front-end' => [
                                            'html' => 'HTML',
                                            'css' => 'CSS',
                                            'sass' => 'SASS',
                                            'js' => 'JS',
                                            'other' => 'Other'
                                        ],
                                    'miscellaneous' => [
                                            'agile' => 'Agile',
                                            'sysadmin' => 'Admin',
                                            'devops' => 'Dev Ops',
                                            'other' => 'Other'
                                            ],
                                        ], null, ['placeholder' => 'Pick a category...', 'class' => "styled-select required"]) !!}

                        @if ($errors->has('category'))
                            <span class="color form-description">
                                <strong>{{$errors->first('category')}} </strong>
                            </span>
                        @else
                            <span class="form-description"> Please choose the appropriate section so easily search for your question . </span> 
                        @endif
            
                        {{-- <span class="form-description">Please choose the appropriate section so easily search for your question .</span> --}}
                    </p>
                </div>


                <!-- Description --> 
                <div id="form-textarea">
                    <p>
                        {!!Form::label('description', 'Details', ['id'=>'question_tags', 'class' => 'required']);!!}
                        {!!Form::textarea('description', '', ['id'=>'question-title', 'class' => 'input', 'cols'=>'58','cols'=>'8', 'data-aria-required' => 'true']);!!}
                      <!--  <span class="form-description">Type the description thoroughly and in detail .</span> -->

                        <!--Gestion d'erreur -->
                    @if ($errors->has('description'))
                         <span class="color form-description">
                        <strong>{{$errors->first('description')}} </strong>
                            </span>
                        @else
                            <span class="form-description"> Type the description thoroughly and in detail . </span>
                        @endif
                    </p>

                    

                </div>

                <!--Summit -->
                <p class="form-submit">
                    {!!Form::submit('Publish Your Question', ["id" => "publish-question", "class" => "button color small submit"]);!!}
                </p>
            {!! Form::close() !!}

            {{-- <form action="{{ action("QuestionControler@store")}}" method="post"> --}}
                {{-- @csrf --}}
                {{-- <div class="form-inputs clearfix"> --}}




                    {{--<label>Attachment</label>--}}

                    {{--<div class="fileinputs">--}}

                        {{--<input type="file" class="file">--}}

                        {{--<div class="fakefile">--}}

                            {{--<button type="button" class="button small margin_0">Select file</button>--}}

                            {{--<span><i class="icon-arrow-up"></i>Browse</span>--}}

                        {{--</div>--}}

                    {{--</div>--}}



                {{-- </div> --}}

            {{-- </form> --}}

        </div>

    </div><!-- End page-content -->



@endsection
