<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
    //return view('homepage');
//});

// Route::get('/poser-une-question', function () {
//     return view('questions/create');
// });

Auth::routes();

Route::resource('question', 'QuestionController')->only([
    'index', 'create', 'store', 'show'
]);

Route::resource('answer', 'AnswerController')->only([
    'store', 'show', 'create', 'index'
 ]);

Route::get('/', 'HomeController@index')->name('home');

Route::get('/Login', function(){
    return view('login');
});

Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );

Route::post('upvote/answer', 'UpvoteController@store');


